#include "dummy_macro_tests.h"
#include "demo.h"

#define ALLOC(X) (X*)malloc(sizeof(X) + 1)
#define ALLOCARR(X, SIZE) (X*)malloc(sizeof(X) * SIZE + 1)
#define CREATE_FREEARR(TYPE) \
void freearr_##TYPE(TYPE **arr)\
{\
    int i = 0;\
    TYPE *_temp = arr[0];\
    while(_temp != NULL) \
    {\
        free(_temp);\
        _temp = arr[++i];\
    }\
}\

#define CREATE_DEFAULT_COMPARATOR(TYPENAME, PARAM1, PARAM2) \
int TYPENAME##_compare(TYPENAME *v1,TYPENAME *v2)\
{\
    return !(v1->PARAM1 == v2->PARAM1 && v1->PARAM2 == v2->PARAM2);\
}\

#define CREATE_DEFAULT_PRINT(TYPENAME, PARAM1, PARAM2, PARAM3) \
void TYPENAME##_print(TYPENAME *v)\
{\
    printf("\n\t"#TYPENAME"{\n\t\t"#PARAM1" %d\n\t\t"#PARAM2" %d\n\t\t"#PARAM3" %d\n\t}\n", v->PARAM1, v->PARAM2, v->PARAM3);\
}\

#define CREATE_PRINTER_ARR(TYPE, PRINTER) \
void printarr_##TYPE(TYPE **arr)\
{\
    int i = 0;\
    TYPE *_temp = arr[0];\
    while(_temp != NULL) \
    {\
        PRINTER(_temp);;\
        _temp  = arr[++i];\
    }\
}\


CREATE_DEFAULT_COMPARATOR(Guest, arrive_time, exit_time);
CREATE_DEFAULT_PRINT(Guest, arrive_time, exit_time, times_watched);
CREATE_DEFAULT_COMPARATOR(Time_interval, begin, end);
CREATE_DEFAULT_PRINT(Time_interval, begin, end, guests_count);
CREATE_FREEARR(Guest);
CREATE_FREEARR(Time_interval);
CREATE_PRINTER_ARR(Guest, Guest_print); 
CREATE_PRINTER_ARR(Time_interval, Time_interval_print); 

#define INITIAL_TEST_B(TYPENAME, VALNAME, PARAM1, PARAM2)\
TYPENAME *VALNAME = ALLOC(TYPENAME);\
ptr = (int*)VALNAME;\
*ptr++ = PARAM1;\
*ptr = PARAM2;\
    TESTCASE(\
        #TYPENAME" creating test",\
        TESTANYPUREFUNCRET(new_##VALNAME, TYPENAME##_compare, TYPENAME##_print, VALNAME, PARAM1, PARAM2);\
    );\

#define REBUILD(VALNAME, PARAM1, PARAM2)\
free(VALNAME);\
VALNAME = new_##VALNAME(PARAM1, PARAM2);\

void Timeline_print(Timeline *timeline)
{
    printf("[\n");
    for (int i = 0; i < timeline->size; ++i)
        printf(" %d ", timeline->moments[i]); 
    printf("\n");
    for (int i = 0; i < timeline->size; ++i)
        printf(" %d ", timeline->guests_count[i]); 
    printf("\n]\n");
}

int main ()
{
    int *ptr = NULL;
    INITIAL_TEST_B(Guest, guest, 0, 1);
    INITIAL_TEST_B(Time_interval, time_interval, 0, 1);
    TESTCASE(
        "Guest in interval",
        TESTFUNCRET(guest_in_interval, int_simplecompare, 1, time_interval, guest);
        REBUILD(guest, 2, 3);
        TESTFUNCRET(guest_in_interval, int_simplecompare, 0, time_interval, guest);
        REBUILD(guest, 0, 4);
        TESTFUNCRET(guest_in_interval, int_simplecompare, 1, time_interval, guest);
    );
    REBUILD(time_interval, 1, 4);
    Guest **guests_arr = ALLOCARR(Guest*, 4);
    guests_arr[0] = new_guest(5,7);
    guests_arr[1] = new_guest(2,3);
    guests_arr[2] = new_guest(1,2);
    guests_arr[3] = NULL;
    TESTCASE(
        "Guests count in interval",
        TESTFUNCRET(guests_in_interval, int_simplecompare, 1, time_interval, guests_arr, 2);
    );
    Timeline *timeline = new_timeline(10, 2);
    timeline->guests_count[1] = 2;
    timeline->guests_count[2] = 2;
    timeline->guests_count[3] = 2;
    REBUILD(time_interval, 2, 6);
    TESTCASE(
        "New time interval from timeline",
        printf("Using timeline:");
        Timeline_print(timeline);
        ptr = &(timeline->moments[1]);
        TESTANYFUNCRET(new_time_interval_from_timeline, Time_interval_compare, Time_interval*, Time_interval_print, time_interval, timeline, &ptr);
    );
    free(timeline);
    timeline = new_timeline_fromarr(guests_arr, 3);
    printarr_Guest(guests_arr);
    Timeline_print(timeline);
    Time_interval** time_interval_array = parse_timeline_to_tinterval_array(timeline);
    printarr_Time_interval(time_interval_array); 
    int *nums = guests_nums_interval(guests_arr, time_interval_array[1]); 
    printf("%d\n", *nums); 

    freearr_Guest(guests_arr); 
    freearr_Time_interval(time_interval_array);
    free(timeline);
    free(time_interval);
    free(guest);
    return 0;
}

#undef REBUILD
#undef ALLOC
#undef ALLOCARR
#undef INITIAL_TEST_B
#undef CREATE_DEFAULT_PRINT
#undef CREATE_DEFAULT_COMPARATOR
