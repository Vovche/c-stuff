#include "demo.h"

#define ALLOC(X) (X*)malloc(sizeof(X) + 1)
#define ALLOCARR(X, SIZE) (X*)malloc(sizeof(X) * SIZE + 1)

#define CREATE_DEFAULT_INITIALIZER_B(TYPENAME, VALNAME, PARAM1, PARAM2, PARAM3, DEFAULT) \
TYPENAME* new_##VALNAME(int PARAM1, int PARAM2)\
{\
    TYPENAME *VALNAME = ALLOC(TYPENAME);\
    VALNAME->PARAM1 = PARAM1;\
    VALNAME->PARAM2 = PARAM2;\
    VALNAME->PARAM3 = DEFAULT;\
    return VALNAME;\
}\

CREATE_DEFAULT_INITIALIZER_B(Guest, guest, arrive_time, exit_time, times_watched, 0);
CREATE_DEFAULT_INITIALIZER_B(Time_interval, time_interval, begin, end, guests_count, 0);

Timeline* new_timeline(int duration, int interval)
{
    Timeline *timeline = ALLOC(Timeline);
    ++duration;
    duration += duration % interval;
    timeline->size = duration / interval;
    timeline->moments = ALLOCARR(int, timeline->size);
    timeline->guests_count = ALLOCARR(int, timeline->size);
    for (int i = 0; i <= duration; i += interval)
    {
        timeline->moments[i / interval] = i;
        timeline->guests_count[i / interval] = 0;
    }
    return timeline;
}

void delete_timeline(Timeline *timeline)
{
    free(timeline->moments);
    free(timeline->guests_count);
    free(timeline);
}

int guest_in_interval(Time_interval *interval, Guest *guest)
{
   return !(guest->exit_time < interval->begin || guest->arrive_time >interval->end); 
}

int guests_in_interval(Time_interval *interval, Guest **guests, int guests_count)
{
    int guests_in = 0;
    for (int i = 0; i < guests_count; ++i)
        if (guest_in_interval(interval, guests[i]))
            ++guests_in;
    return guests_in;
}

Time_interval* new_time_interval_from_timeline(Timeline *timeline, int **currentMoment)
{
    int weight = timeline->guests_count[*currentMoment - timeline->moments];
    int *cursor = *currentMoment;
    while (weight == timeline->guests_count[cursor - timeline->moments])
        ++cursor;
   Time_interval *interval = new_time_interval(**currentMoment, *(cursor - 1)); 
   interval->guests_count = weight;
   *currentMoment = cursor;
   return interval;
}

#define TIME_OFFSET(TIMELINE, MOMENT) (MOMENT - timeline->moments)
#define TIME_VALUE(TIMELINE, MOMENT) TIMELINE->guests_count[TIME_OFFSET(TIMELINE, MOMENT)]
Time_interval** parse_timeline_to_tinterval_array(Timeline *timeline)
{
    int *cursor = timeline->moments;
    int i = 0;
    Time_interval **interval_array = ALLOCARR(Time_interval*, timeline->size);
    while (cursor != (timeline->moments + timeline->size))
    {
        if (TIME_VALUE(timeline, cursor) == 0 && cursor < (timeline->moments + timeline->size))
        {
            ++cursor;
            continue;
        }
        if (cursor > timeline->moments + timeline->size)
            break;
        interval_array[i++] = new_time_interval_from_timeline(timeline, &cursor);
    }
    interval_array[i] = NULL;
    return interval_array;
}

Timeline* new_timeline_fromarr(Guest** guest_arr, int size)
{
    int min_time = guest_arr[0]->arrive_time;
    int max_time = min_time;
    int min_step = guest_arr[0]->exit_time - guest_arr[0]->arrive_time;
    for (int i = 0; i < size; ++i)
    {
        if (guest_arr[i]->arrive_time < min_time)
            min_time = guest_arr[i]->arrive_time;
        if (guest_arr[i]->exit_time > max_time)
            max_time = guest_arr[i]->exit_time;
        if (guest_arr[i]->exit_time - guest_arr[i]->arrive_time != 0 && guest_arr[i]->exit_time - guest_arr[i]->arrive_time < min_step)
            min_step = guest_arr[i]->exit_time - guest_arr[i]->arrive_time;
    }
    Timeline *timeline = new_timeline(max_time, min_step);
    for (int i = 0; i < size; ++i)
        for  (int j = guest_arr[i]->arrive_time; j <= guest_arr[i]->exit_time; j += min_step)
        {
            timeline->guests_count[j] = timeline->guests_count[j] + 1;
        }
    return timeline;
}

#define SIZE(TYPE)\
int TYPE##_monearrsize(TYPE* arr)\
{\
    int size = 0;\
    while(*arr != -1)\
    {\
        ++size;\
        ++arr;\
    }\
    return size;\
}\

#define ASIZE(TYPE)\
int TYPE##_ptrarrsize(TYPE** arr)\
{\
    int size = 0;\
    while(*arr != NULL)\
    {\
        ++size;\
        ++arr;\
    }\
    return size;\
}\

ASIZE(Guest);
SIZE(int);

int* guests_nums_interval(Guest** guest_arr, Time_interval* interval)
{
    int *out = ALLOCARR(int, Guest_ptrarrsize(guest_arr));
    out[0] = -1;
    for (int i = 0; i < Guest_ptrarrsize(guest_arr); ++i)
        if (guest_in_interval(interval, guest_arr[i]))
        {
            int j = int_monearrsize(out);
            out[j++] = i;
            out[j] = -1; 
        }
    return out;
}

#undef TIME_OFFSET
#undef TIME_VALUE
#undef ALLOC
#undef ALLOCARR
#undef CREATE_DEFAULT_INITIALIZER_B
