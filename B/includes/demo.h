#ifndef DEMO
#define DEMO

#include <stdio.h>
#include <malloc.h>

typedef struct dummy_guest
{
    int arrive_time;
    int exit_time;
    int times_watched;
} Guest;

typedef struct dummy_timeline
{
    int *moments;
    int *guests_count;
    int size;
} Timeline;

typedef struct dummy_time_interval
{
    int begin;
    int end;
    int guests_count;
} Time_interval;

/*
 * defined in stuff
*/

Guest* new_guest(int arrive_time, int exit_time);
Time_interval* new_time_interval(int begin, int end);
Timeline* new_timeline(int duration, int interval);

void delete_timeline(Timeline *timeline);

int guest_in_interval(Time_interval *interval, Guest *guest);
int guests_in_interval(Time_interval *interval, Guest **guests, int guests_count);

// changes current moment to last dot of interval + 1
Time_interval* new_time_interval_from_timeline(Timeline *timeline, int **currentMoment);
Time_interval** parse_timeline_to_tinterval_array(Timeline *timeline);
Timeline* new_timeline_fromarr(Guest** guest_arr, int size);
int* guests_nums_interval(Guest** guest_arr, Time_interval* interval);
//+

//bubble by guests_count
void sort_interval_array(Time_interval** time_interval_array);
#endif
