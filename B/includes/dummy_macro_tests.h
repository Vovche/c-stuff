#ifndef DUMMY_MACRO_TESTS_H
#define DUMMY_MACRO_TESTS_H

#include <stdio.h>
#include <stdlib.h>

#define TESTCASE(DESK, ...) \
printf("TESTCASE\n");\
printf("Description: \n\t%s\n", DESK);\
__VA_ARGS__;

//int return type only :(
#define TESTFUNCRET(FUN_NAME, COMPARATOR, EXPECTED_VAL, ...) \
printf("Test:\n");\
printf("\tExpected: %d\n", EXPECTED_VAL); \
printf("\tActual: %d\n", FUN_NAME(__VA_ARGS__));\
if (COMPARATOR(EXPECTED_VAL, FUN_NAME(__VA_ARGS__)) != 0)\
{\
    printf("Result: Fuck:(\n");\
    exit(-1);\
}\
printf("Result: OK\n");\

//pure functions only!:c
#define TESTANYPUREFUNCRET(FUN_NAME, COMPARATOR, RET_TO_SCREEN, EXPECTED_VAL, ...) \
printf("Test:\n");\
printf("\tExpected: "); \
RET_TO_SCREEN(EXPECTED_VAL); \
printf("\tActual: ");\
/*Memory leak :c*/ \
RET_TO_SCREEN(FUN_NAME(__VA_ARGS__)); \
if (COMPARATOR(EXPECTED_VAL, FUN_NAME(__VA_ARGS__)) != 0)\
{\
    printf("Result: Fuck:(\n");\
    exit(-1);\
}\
printf("Result: OK\n");\

#define TESTANYFUNCRET(FUN_NAME, COMPARATOR, TYPENAME, RET_TO_SCREEN, EXPECTED_VAL, ...) \
printf("Test:\n");\
printf("\tExpected: "); \
RET_TO_SCREEN(EXPECTED_VAL); \
printf("\tActual: ");\
TYPENAME temp = FUN_NAME(__VA_ARGS__);\
RET_TO_SCREEN(temp);\
if (COMPARATOR(EXPECTED_VAL, temp) != 0)\
{\
    printf("Result: Fuck:(\n");\
    exit(-1);\
}\
printf("Result: OK\n");\

/*
 * Comparators
*/

#define CREATE_EQ_COMP(X) \
X X##_simplecompare (X a, X b) { return a != b; }

CREATE_EQ_COMP(int);

/*
 * Printers
*/

#undef CREATE_EQ_COMP
#endif
