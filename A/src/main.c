#include "demo.h"

int main ()
{
    int size = 0;
    int key = 0;
    int val = 0;

    scanf("%d", &size);
    dtable *table = new_dtable(size);
    for (int i = 0; i < size; ++i)
    {
        scanf("%d", &key);
        dtable_add(table, key);
    }
    scanf("%d", &size);
    for (int i = 0; i < size; ++i)
    {
        scanf("%d", &key);
        if (i != 0)
            printf(" ");
        val = dtable_get(table, key); 
        printf("%d", val == -1?0:val);
    }
    printf("\n");
    delete_dtable(table);
    return 0;
}
