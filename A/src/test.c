#include "dummy_macro_tests.h"
#include "demo.h"

int main ()
{
    dtable *table = new_dtable(2);
    printf("\nEmpty table created\n\n");
    TESTCASE(
        "dtable_conteinkey test:",
        TESTFUNCRET(dtable_containkey,int_simplecompare, 0, table, 1);
    printf("\t...Adding 1 to table\n");
    dtable_add(table, 1);
        TESTFUNCRET(dtable_containkey,int_simplecompare, 1, table, 1);
    );
    TESTCASE(
        "dtable_get test:",
        TESTFUNCRET(dtable_get,int_simplecompare, 1, table, 1);
        TESTFUNCRET(dtable_get,int_simplecompare, -1, table, 0);
    printf("\t...Adding 1 to table\n");
    dtable_add(table, 1);
        TESTFUNCRET(dtable_get,int_simplecompare, 2, table, 1);
    printf("\t...Adding 2 to table\n");
    dtable_add(table, 2);
        TESTFUNCRET(dtable_get,int_simplecompare, 1, table, 2);
    );
    delete_dtable(table);
    return 0;
}
