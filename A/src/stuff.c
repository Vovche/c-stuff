#include "demo.h"

int dtable_keynum(dtable *table, int key)
{
    int *current_key = table->keys;
    while (*current_key != -1)
        if (key == *current_key++)
            return (current_key - 1) - table->keys;
    return -1;
}

dtable* new_dtable(int size)
{
    dtable *table = (dtable*) malloc(sizeof(dtable) + 1);
    table->keys = (int*) malloc(sizeof(int) * (size + 1) + 1);
    table->values = (int*) malloc(sizeof(int) * size + 1);
    table->keys[0] = -1;
    while (size)
        table->values[size--] = 0;
    return table;
}

int dtable_containkey(dtable *table, int key)
{
    int *current_key = table->keys;
    while (*current_key != -1)
        if (key == *current_key++)
            return 1;
    return 0;
}

void dtable_add(dtable *table, int key)
{
    if (dtable_containkey(table, key))
    {
        table->values[dtable_keynum(table, key)] += 1;
        return;
    }
    int *current_key = table->keys;
    while (*current_key != -1)
        ++current_key;
    *current_key = key;
    *(current_key + 1) = -1;
    table->values[dtable_keynum(table, key)] += 1;
}

int dtable_get(dtable *table, int key)
{
    if (dtable_containkey(table, key))
    {
        return table->values[dtable_keynum(table, key)];
    }
    return -1; 
}
void delete_dtable(dtable *table)
{
    free(table->keys);
    free(table->values);
    free(table);
}
