#ifndef DEMO
#define DEMO

#include <stdio.h>
#include <malloc.h>

typedef struct dummy_table
{
    int *keys;
    int *values;
} dtable;

/*
 * defined in stuff
*/

dtable* new_dtable(int size);
int dtable_containkey(dtable *table, int key);
void dtable_add(dtable *table, int key);
// -1 if not found
int dtable_get(dtable *table, int key);
void delete_dtable(dtable *table);
#endif
