#include <stdio.h>
#include <malloc.h>


typedef struct dummy_table
{
    int *keys;
    int *values;
} dtable;

/*
 * defined in stuff
*/

dtable* new_dtable(int size);
int dtable_containkey(dtable *table, int key);
void dtable_add(dtable *table, int key);
// -1 if not found
int dtable_get(dtable *table, int key);
void delete_dtable(dtable *table);

int dtable_keynum(dtable *table, int key)
{
    int *current_key = table->keys;
    while (*current_key != -1)
        if (key == *current_key++)
            return (current_key - 1) - table->keys;
    return -1;
}

dtable* new_dtable(int size)
{
    dtable *table = (dtable*) malloc(sizeof(dtable) + 1);
    table->keys = (int*) malloc(sizeof(int) * (size + 1) + 1);
    table->values = (int*) malloc(sizeof(int) * size + 1);
    table->keys[0] = -1;
    while (size)
        table->values[size--] = 0;
    return table;
}

int dtable_containkey(dtable *table, int key)
{
    int *current_key = table->keys;
    while (*current_key != -1)
        if (key == *current_key++)
            return 1;
    return 0;
}

void dtable_add(dtable *table, int key)
{
    if (dtable_containkey(table, key))
    {
        table->values[dtable_keynum(table, key)] += 1;
        return;
    }
    int *current_key = table->keys;
    while (*current_key != -1)
        ++current_key;
    *current_key = key;
    *(current_key + 1) = -1;
    table->values[dtable_keynum(table, key)] += 1;
}

int dtable_get(dtable *table, int key)
{
    if (dtable_containkey(table, key))
    {
        return table->values[dtable_keynum(table, key)];
    }
    return -1; 
}
void delete_dtable(dtable *table)
{
    free(table->keys);
    free(table->values);
    free(table);
}

int main ()
{
    int size = 0;
    int key = 0;
    int val = 0;

    scanf("%d", &size);
    dtable *table = new_dtable(size);
    for (int i = 0; i < size; ++i)
    {
        scanf("%d", &key);
        dtable_add(table, key);
    }
    scanf("%d", &size);
    for (int i = 0; i < size; ++i)
    {
        scanf("%d", &key);
        if (i != 0)
            printf(" ");
        val = dtable_get(table, key); 
        printf("%d", val == -1?0:val);
    }
    printf("\n");
    delete_dtable(table);
    return 0;
}
