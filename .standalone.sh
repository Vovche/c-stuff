#!/bin/zsh

echo '#include <stdio.h>' > Standalone.c
echo '#include <malloc.h>' >> Standalone.c
cat includes/demo.h | grep -v '#' >> Standalone.c
cat src/stuff.c | grep -v '#' >> Standalone.c
cat src/main.c | grep -v '#' >> Standalone.c
